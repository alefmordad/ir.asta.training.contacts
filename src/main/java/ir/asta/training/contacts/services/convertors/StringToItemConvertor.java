package ir.asta.training.contacts.services.convertors;

import java.text.ParseException;

import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.ProductEntity;

public class StringToItemConvertor implements Convertor<String, ItemEntity> {

	@Override
	public ItemEntity convertFromTo(String from) throws ParseException {
		ItemEntity itemEntity = new ItemEntity();
		String[] fromParts = from.split(":");
		String productCode = fromParts[0];
		ProductEntity productEntity = new ProductEntity();
		productEntity.setCode(productCode);
		itemEntity.setProduct(productEntity);
		itemEntity.setQuantity(Integer.valueOf(fromParts[1]));
		return itemEntity;
	}

	@Override
	public String convertToFrom(ItemEntity to) throws ParseException {
		return "" + to.getProduct().getCode() + ":" + to.getQuantity();
	}

}
