package ir.asta.training.contacts.services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import ir.asta.training.contacts.entities.ContactEntity;
import ir.asta.training.contacts.manager.ContactManager;
import ir.asta.training.contacts.services.ContactService;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("contactService")
public class ContactServiceImpl implements ContactService {

	@Inject
	ContactManager manager;
	
	@Context
	HttpServletRequest request;
	
	@Override
	public ContactEntity load(Long id) {
		return manager.load(id);
	}

	@Override
	public ActionResult<Long> save(ContactEntity entity) {
		manager.save(entity);
		return new ActionResult<Long>(true, "New contact saved successfully.", entity.getId());
	}
	
	@Override
	public ActionResult<Long> update(ContactEntity entity) {
		manager.update(entity);
		return new ActionResult<Long>(true, "contact updated successfully.", entity.getId());
	}

	@Override
	public List<ContactEntity> findAll() {
		return manager.findAll();
	}
	
	@Override
	public List<ContactEntity> search() {
		return manager.search(request.getParameterMap());
	}

	@Override
	public ActionResult<Long> delete(long id) {
		manager.delete(id);
		return new ActionResult<Long>(true, "deleted", id);
	}

	
}
