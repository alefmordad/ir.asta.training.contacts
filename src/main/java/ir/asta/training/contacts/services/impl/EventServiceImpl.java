package ir.asta.training.contacts.services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import ir.asta.training.contacts.entities.EventEntity;
import ir.asta.training.contacts.manager.EventManager;
import ir.asta.training.contacts.services.EventService;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("eventService")
public class EventServiceImpl implements EventService {

	@Inject
	private EventManager eventManager;
	
	@Context
	HttpServletRequest request;
	
	@Override
	public ActionResult<Long> save(EventEntity e) {
		eventManager.save(e);
		return new ActionResult<Long>(true, "event saved", e.getId());
	}

	@Override
	public EventEntity load(long id) {
		return eventManager.load(id);
	}

	@Override
	public List<EventEntity> findAll() {
		return eventManager.findAll();
	}

	@Override
	public ActionResult<Long> update(EventEntity e) {
		eventManager.update(e);
		return new ActionResult<Long>(true, "event updated", e.getId());
	}

	@Override
	public ActionResult<Long> delete(long id) {
		eventManager.delete(id);
		return new ActionResult<Long>(true, "deleted", id);
	}
	
	@Override
	public List<EventEntity> search() {
		return eventManager.search(request.getParameterMap());
	}

}
