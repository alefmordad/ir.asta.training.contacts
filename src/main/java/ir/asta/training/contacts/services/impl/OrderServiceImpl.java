package ir.asta.training.contacts.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.OrderEntity;
import ir.asta.training.contacts.manager.OrderManager;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;
import ir.asta.training.contacts.model.dto.OrderDto;
import ir.asta.training.contacts.services.OrderService;
import ir.asta.training.contacts.services.convertors.StringToDateConvertor;
import ir.asta.training.contacts.services.convertors.StringToItemConvertor;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("orderService")
public class OrderServiceImpl implements OrderService {

	@Inject
	private OrderManager orderManager;
		
	@Override
	public ActionResult<OrderDto> saveOrder(String purchaseTimeString, String itemsString) throws ParseException {
		if (!purchaseTimeString.matches("[0-9]{3}[1-9]-[0-9][1-9]-[0-9][1-9]")) {
			throw new BadArgumentException(purchaseTimeString + " is an invalid date");
		}
		if (!itemsString.matches("([A-Za-z0-9]+:[1-9][0-9]*)(,[A-Za-z0-9]+:[1-9][0-9]*)*")) {
			throw new BadArgumentException(itemsString + " is not valid to be a list of items");
		}
		StringToItemConvertor stringToItemConvertor = new StringToItemConvertor();
		Date purchaseTime = new StringToDateConvertor().convertFromTo(purchaseTimeString);
		List<ItemEntity> items = new ArrayList<>();
		for (String itemString : itemsString.split(",")) {
			items.add(stringToItemConvertor.convertFromTo(itemString));
		}
		OrderEntity orderEntity = orderManager.saveOrder(purchaseTime, items);
		OrderDto orderDto = new OrderDto(orderEntity);
		return new ActionResult<OrderDto>(true, "order saved", orderDto);
	}

	@Override
	public ActionResult<List<OrderDto>> findAll() {
		List<OrderEntity> orders = orderManager.findAll();
		List<OrderDto> orderDtos = new ArrayList<>();
		orders.forEach(order -> orderDtos.add(new OrderDto(order)));
		return new ActionResult<List<OrderDto>>(orderDtos);
	}

}
