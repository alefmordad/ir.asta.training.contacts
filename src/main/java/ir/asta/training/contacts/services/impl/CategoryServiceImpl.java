package ir.asta.training.contacts.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import ir.asta.training.contacts.entities.CategoryEntity;
import ir.asta.training.contacts.manager.CategoryManager;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;
import ir.asta.training.contacts.model.Sale;
import ir.asta.training.contacts.model.dto.CategoryDto;
import ir.asta.training.contacts.services.CategoryService;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Inject
	private CategoryManager categoryManager;

	@Override
	public ActionResult<Long> save(String subject) {
		try {
			CategoryEntity categoryEntity = new CategoryEntity();
			categoryEntity.setSubject(subject);
			Long id = categoryManager.save(categoryEntity);
			return new ActionResult<Long>(true, "category saved", id);
		} catch (Exception e) {
			throw new BadArgumentException("subject '" + subject + "' is taken");
		}
	}

	@Override
	public ActionResult<List<CategoryDto>> findAll() {
		List<CategoryEntity> categories = categoryManager.findAll();
		List<CategoryDto> dtos = new ArrayList<>();
		categories.forEach(c -> dtos.add(new CategoryDto(c)));
		return new ActionResult<List<CategoryDto>>(dtos);
	}

	@Override
	public ActionResult<List<Sale>> getTotalSale(String subject) {
		List<Sale> sales = categoryManager.getTotalSales(subject);
		return new ActionResult<List<Sale>>(sales);
	}

}
