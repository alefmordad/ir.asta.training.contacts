package ir.asta.training.contacts.services.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.manager.ItemManager;
import ir.asta.training.contacts.model.dto.ItemDto;
import ir.asta.training.contacts.services.ItemService;
import ir.asta.training.contacts.services.convertors.StringToDateConvertor;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("itemService")
public class ItemServiceImpl implements ItemService {

	@Inject
	private ItemManager itemManager;

	@Override
	public ActionResult<List<ItemDto>> findAll() {
		List<ItemEntity> items = itemManager.findAll();
		List<ItemDto> itemDtos = new ArrayList<>();
		items.forEach(item -> itemDtos.add(new ItemDto(item)));
		return new ActionResult<List<ItemDto>>(itemDtos);
	}

	@Override
	public ActionResult<List<ItemDto>> findInInterval(String fromDateString, String toDateString) throws ParseException {
		StringToDateConvertor stringToDateConvertor = new StringToDateConvertor();
		Date fromDate = stringToDateConvertor.convertFromTo(fromDateString);
		Date toDate = stringToDateConvertor.convertFromTo(toDateString);
		List<ItemEntity> itemEntities = itemManager.findInInterval(fromDate, toDate);
		List<ItemDto> itemDtos = new ArrayList<>();
		itemEntities.forEach(item -> itemDtos.add(new ItemDto(item)));
		return new ActionResult<List<ItemDto>>(itemDtos);
	}

}
