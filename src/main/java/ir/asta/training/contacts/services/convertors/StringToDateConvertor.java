package ir.asta.training.contacts.services.convertors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringToDateConvertor implements Convertor<String, Date> {

	@Override
	public Date convertFromTo(String from) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		return sdf.parse(from);
	}

	@Override
	public String convertToFrom(Date to) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		return sdf.format(to);
	}

}
