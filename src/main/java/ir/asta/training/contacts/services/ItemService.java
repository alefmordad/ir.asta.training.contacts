package ir.asta.training.contacts.services;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.training.contacts.model.dto.ItemDto;
import ir.asta.wise.core.datamanagement.ActionResult;

@Path("item")
public interface ItemService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("findAll")
	ActionResult<List<ItemDto>> findAll();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("find-items-in-range/{fromDate}/{toDate}")
	ActionResult<List<ItemDto>> findInInterval(@PathParam("fromDate") String fromDateString,
			@PathParam("toDate") String toDateString) throws ParseException;

}
