package ir.asta.training.contacts.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.training.contacts.model.dto.CategoryDto;
import ir.asta.training.contacts.model.dto.ProductDto;
import ir.asta.wise.core.datamanagement.ActionResult;

@Path("product")
public interface ProductService {
	
	@GET
	@Path("save-product/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<Long> save(@PathParam("code") String code);

	@GET
	@Path("set-product-categories/{productCode}")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<List<CategoryDto>> setCategories(@PathParam("productCode") String productCode);
	
	@GET
	@Path("get-product-categories/{productCode}")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<List<CategoryDto>> getCategories(@PathParam("productCode") String productCode);
	
	@GET
	@Path("findAll")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<List<ProductDto>> findAll();
	
	@GET
	@Path("get-product-by-code/{code}")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<ProductDto> loadByCode(@PathParam("code") String code);
	
}
