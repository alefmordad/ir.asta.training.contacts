package ir.asta.training.contacts.services.convertors;

import java.text.ParseException;

public interface Convertor<F, T> {

	T convertFromTo(F from) throws ParseException;
	F convertToFrom(T to) throws ParseException;

}