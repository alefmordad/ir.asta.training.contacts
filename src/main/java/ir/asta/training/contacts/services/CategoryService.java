package ir.asta.training.contacts.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.training.contacts.model.Sale;
import ir.asta.training.contacts.model.dto.CategoryDto;
import ir.asta.wise.core.datamanagement.ActionResult;

@Path("category")
public interface CategoryService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-category/{subject}")
	ActionResult<Long> save(@PathParam("subject") String subject);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("findAll")
	ActionResult<List<CategoryDto>> findAll();
	
	@GET
	@Path("total-sales/{subject}")
	@Produces(MediaType.APPLICATION_JSON)
	ActionResult<List<Sale>> getTotalSale(@PathParam("subject") String subject);
	
}
