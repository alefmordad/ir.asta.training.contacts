package ir.asta.training.contacts.services;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.training.contacts.model.dto.OrderDto;
import ir.asta.wise.core.datamanagement.ActionResult;

@Path("order")
public interface OrderService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("save-order/{orderDate}/{items}")
	ActionResult<OrderDto> saveOrder(@PathParam("orderDate") String purchaseTimeString,
			@PathParam("items") String itemsString) throws ParseException;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("findAll")
	ActionResult<List<OrderDto>> findAll();
	
}
