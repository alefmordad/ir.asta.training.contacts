package ir.asta.training.contacts.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ir.asta.training.contacts.entities.EventEntity;
import ir.asta.wise.core.datamanagement.ActionResult;

@Path("event")
public interface EventService {

	@POST
	@Path("save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ActionResult<Long> save(EventEntity e);
	
	@GET
	@Path("load/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public EventEntity load(@PathParam("id") long id);
	
	@GET
	@Path("findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventEntity> findAll();
	
	@PUT
	@Path("update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ActionResult<Long> update(EventEntity e);
	
	@DELETE
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ActionResult<Long> delete(@PathParam("id") long id);
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventEntity> search();
	
}
