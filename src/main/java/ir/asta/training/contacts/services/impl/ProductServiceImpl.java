package ir.asta.training.contacts.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import ir.asta.training.contacts.entities.CategoryEntity;
import ir.asta.training.contacts.entities.ProductEntity;
import ir.asta.training.contacts.manager.ProductManager;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;
import ir.asta.training.contacts.model.dto.CategoryDto;
import ir.asta.training.contacts.model.dto.ProductDto;
import ir.asta.training.contacts.services.ProductService;
import ir.asta.wise.core.datamanagement.ActionResult;

@Named("productService")
public class ProductServiceImpl implements ProductService {

	@Inject
	private ProductManager productManager;

	@Context
	private HttpServletRequest request;

	@Override
	public ActionResult<Long> save(String code) {
		try {
			ProductEntity product = new ProductEntity();
			product.setCode(code);
			Long id = productManager.save(product);
			return new ActionResult<Long>(true, "product saved", id);
		} catch (Exception e) {
			throw new BadArgumentException("code '" + code + "' is taken");
		}
	}

	@Override
	public ActionResult<List<CategoryDto>> setCategories(String productCode) {
		List<CategoryEntity> categories = productManager.setCategories(productCode, request.getParameterMap());
		List<CategoryDto> categoryDtos = new ArrayList<>();
		categories.forEach(category -> categoryDtos.add(new CategoryDto(category)));
		return new ActionResult<List<CategoryDto>>("categories set", categoryDtos);
	}

	public ActionResult<List<CategoryDto>> getCategories(String productCode) {
		List<CategoryEntity> categories = productManager.getCategories(productCode);
		List<CategoryDto> categoryDtos = new ArrayList<>();
		categories.forEach(category -> categoryDtos.add(new CategoryDto(category)));
		return new ActionResult<List<CategoryDto>>(categoryDtos);
	}

	@Override
	public ActionResult<List<ProductDto>> findAll() {
		List<ProductEntity> products = productManager.findAll();
		List<ProductDto> productDtos = new ArrayList<>();
		products.forEach(product -> productDtos.add(new ProductDto(product)));
		return new ActionResult<List<ProductDto>>(productDtos);
	}

	@Override
	public ActionResult<ProductDto> loadByCode(String code) {
		ProductEntity productEntity = productManager.loadByCode(code);
		ProductDto productDto = new ProductDto(productEntity);
		return new ActionResult<ProductDto>(productDto);
	}

}
