package ir.asta.training.contacts.model.dto;

import ir.asta.training.contacts.entities.ProductEntity;

public class ProductDto {

	private Long id;
	private String code;

	public ProductDto(ProductEntity entity) {
		if (entity != null) {
			setId(entity.getId());
			setCode(entity.getCode());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
