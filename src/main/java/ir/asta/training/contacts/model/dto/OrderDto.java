package ir.asta.training.contacts.model.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.OrderEntity;

public class OrderDto {

	private Long id;
	private List<ItemDto> items;
	private Date purchaseTime;

	{
		items = new ArrayList<>();
	}

	public OrderDto(OrderEntity orderEntity) {
		this.id = orderEntity.getId();
		for (ItemEntity itemEntity : orderEntity.getItems()) {
			this.items.add(new ItemDto(itemEntity));
		}
		this.purchaseTime = orderEntity.getPurchaseTime();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<ItemDto> getItems() {
		return items;
	}

	public void setItems(List<ItemDto> items) {
		this.items = items;
	}

	public Date getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(Date purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

}
