package ir.asta.training.contacts.model;

public class Sale {

	private String productCode;
	private Integer toalQuantity;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getToalQuantity() {
		return toalQuantity;
	}

	public void setToalQuantity(Integer toalQuantity) {
		this.toalQuantity = toalQuantity;
	}

}
