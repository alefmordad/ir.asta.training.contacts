package ir.asta.training.contacts.model.dto;

import ir.asta.training.contacts.entities.ItemEntity;

public class ItemDto {

	private Long id;
	private ProductDto product;
	private Integer quantity;
	
	public ItemDto(ItemEntity itemEintity) {
		this.id = itemEintity.getId();
		this.product = new ProductDto(itemEintity.getProduct());
		this.quantity = itemEintity.getQuantity();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductDto getProduct() {
		return product;
	}

	public void setProduct(ProductDto product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
