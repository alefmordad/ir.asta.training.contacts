package ir.asta.training.contacts.model.dto;

import ir.asta.training.contacts.entities.CategoryEntity;

public class CategoryDto {

	private Long id;
	private String subject;

	public CategoryDto(CategoryEntity category) {
		this.id = category.getId();
		this.subject = category.getSubject();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
