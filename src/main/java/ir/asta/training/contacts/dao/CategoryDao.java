package ir.asta.training.contacts.dao;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.CategoryEntity;

@Named("categoryDao")
public class CategoryDao {

	@PersistenceContext
	private EntityManager em;

	public void save(CategoryEntity e) {
		em.persist(e);
	}

	public CategoryEntity load(Long id) {
		return em.find(CategoryEntity.class, id);
	}

	public void update(CategoryEntity e) {
		em.merge(e);
	}

	public void delete(Long id) {
		em.remove(this.load(id));
	}

	public CategoryEntity loadBySubject(String categorySubject) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
		Root<CategoryEntity> e = cq.from(CategoryEntity.class);
		cq.where(cb.equal(e.get("subject"), categorySubject));
		TypedQuery<CategoryEntity> query = em.createQuery(cq);
		return query.getResultList().isEmpty() ? null : query.getResultList().get(0);
	}

	public List<CategoryEntity> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
		Root<CategoryEntity> r = cq.from(CategoryEntity.class);
		cq.select(r);
		TypedQuery<CategoryEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

}
