package ir.asta.training.contacts.dao;

import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.OrderEntity;

@Named("orderDao")
public class OrderDao {

	@PersistenceContext
	private EntityManager em;

	public void save(OrderEntity e) {
		em.persist(e);
	}

	public OrderEntity load(Long id) {
		return em.find(OrderEntity.class, id);
	}

	public void update(OrderEntity e) {
		em.merge(e);
	}

	public void delete(Long id) {
		em.remove(this.load(id));
	}
	
	public List<OrderEntity> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<OrderEntity> cq = cb.createQuery(OrderEntity.class);
		Root<OrderEntity> r = cq.from(OrderEntity.class);
		cq.select(r);
		TypedQuery<OrderEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

	public List<OrderEntity> findInInterval(Date fromDate, Date toDate) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<OrderEntity> cq = cb.createQuery(OrderEntity.class);
		Root<OrderEntity> r = cq.from(OrderEntity.class);
		cq.where(cb.between(r.get("purchaseTime"), fromDate, toDate));
		TypedQuery<OrderEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

}
