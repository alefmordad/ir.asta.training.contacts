package ir.asta.training.contacts.dao;

import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.ContactEntity;

@Named("contactDao")
public class ContactDao {

	@PersistenceContext
	private EntityManager em;

	public void save(ContactEntity entity) {
		em.persist(entity);
	}

	public ContactEntity load(Long id) {
		return em.find(ContactEntity.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<ContactEntity> findAll() {
		return em.createQuery("from ContactEntity").getResultList();
	}

	public void update(ContactEntity entity) {
		em.merge(entity);
	}

	public List<ContactEntity> search(Map<String, String[]> params) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ContactEntity> cq = cb.createQuery(ContactEntity.class);
		Root<ContactEntity> r = cq.from(ContactEntity.class);
		for (String param : params.keySet()) {
			cq.where(cb.like(r.get(param), params.get(param)[0] + "%"));
		}
		TypedQuery<ContactEntity> q = em.createQuery(cq);
		return q.getResultList();
	}

	public void delete(long id) {
		em.remove(load(id));
	}

}
