package ir.asta.training.contacts.dao;

import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.OrderEntity;

@Named("itemDao")
public class ItemDao {

	@PersistenceContext
	private EntityManager em;

	public void save(ItemEntity e) {
		em.persist(e);
	}

	public ItemEntity load(Long id) {
		return em.find(ItemEntity.class, id);
	}

	public void update(ItemEntity e) {
		em.merge(e);
	}

	public void delete(Long id) {
		em.remove(this.load(id));
	}

	public List<ItemEntity> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ItemEntity> cq = cb.createQuery(ItemEntity.class);
		Root<ItemEntity> r = cq.from(ItemEntity.class);
		cq.select(r);
		TypedQuery<ItemEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

	public List<ItemEntity> findInInterval(Date fromDate, Date toDate) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ItemEntity> cq = cb.createQuery(ItemEntity.class);
		Root<OrderEntity> r = cq.from(OrderEntity.class);
		cq.where(cb.between(r.get("purchaseTime"), fromDate, toDate));
		cq.select(r.get("items"));
		TypedQuery<ItemEntity> tq = em.createQuery(cq);
		List<ItemEntity> listOfLists = tq.getResultList();
		return listOfLists;
	}

}
