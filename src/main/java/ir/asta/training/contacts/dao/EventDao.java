package ir.asta.training.contacts.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.EventEntity;

@Named("eventDao")
public class EventDao {

	@PersistenceContext
	private EntityManager em;

	public void save(EventEntity e) {
		em.persist(e);
	}

	public EventEntity load(long id) {
		return em.find(EventEntity.class, id);
	}

	public void update(EventEntity e) {
		em.merge(e);
	}

	@SuppressWarnings("unchecked")
	public List<EventEntity> findAll() {
		return em.createQuery("from EventEntity").getResultList();
	}

	public void delete(long id) {
		em.remove(load(id));
	}

	@SuppressWarnings("deprecation")
	public List<EventEntity> search(Map<String, String[]> params) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<EventEntity> cq = cb.createQuery(EventEntity.class);
		Root<EventEntity> r = cq.from(EventEntity.class);
		for (String param : params.keySet()) {
			if (param.contains("Date")) {
				cq.where(cb.equal(r.get(param), new Date(params.get(param)[0])));
			} else {
				cq.where(cb.like(r.get(param), params.get(param)[0] + "%"));
			}
		}
		TypedQuery<EventEntity> q = em.createQuery(cq);
		return q.getResultList();
	}

}
