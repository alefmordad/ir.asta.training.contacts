package ir.asta.training.contacts.dao;

import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ir.asta.training.contacts.entities.CategoryEntity;
import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.ProductEntity;

@Named("productDao")
public class ProductDao {

	@PersistenceContext
	private EntityManager em;

	public void save(ProductEntity e) {
		em.persist(e);
	}

	public ProductEntity load(Long id) {
		return em.find(ProductEntity.class, id);
	}

	public void update(ProductEntity e) {
		em.merge(e);
	}

	public void delete(Long id) {
		em.remove(this.load(id));
	}

	public ProductEntity loadByCode(String productCode) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProductEntity> cq = cb.createQuery(ProductEntity.class);
		Root<ProductEntity> e = cq.from(ProductEntity.class);
		cq.where(cb.equal(e.get("code"), productCode));
		TypedQuery<ProductEntity> query = em.createQuery(cq);
		return query.getResultList().isEmpty() ? null : query.getResultList().get(0);
	}
	
	public List<ProductEntity> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProductEntity> cq = cb.createQuery(ProductEntity.class);
		Root<ProductEntity> r = cq.from(ProductEntity.class);
		cq.select(r);
		TypedQuery<ProductEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

	public List<CategoryEntity> getCategories(String productCode) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
		Root<ProductEntity> r = cq.from(ProductEntity.class);
		cq.where(cb.equal(r.get("code"), productCode));
		cq.select(r.get("categories"));
		TypedQuery<CategoryEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

	public List<ItemEntity> getRelatedItems(ProductEntity productEntity) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ItemEntity> cq = cb.createQuery(ItemEntity.class);
		Root<ItemEntity> r = cq.from(ItemEntity.class);
		cq.where(cb.equal(r.get("product"), productEntity));
		TypedQuery<ItemEntity> tq = em.createQuery(cq);
		return tq.getResultList();
	}

}
