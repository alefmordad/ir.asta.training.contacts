package ir.asta.training.contacts.manager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import ir.asta.training.contacts.dao.CategoryDao;
import ir.asta.training.contacts.entities.CategoryEntity;
import ir.asta.training.contacts.entities.ProductEntity;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;
import ir.asta.training.contacts.model.Sale;

@Named("categoryManager")
@Transactional
public class CategoryManager {

	@Inject
	private CategoryDao categoryDao;
	@Inject
	private ProductManager productManager;

	public Long save(CategoryEntity categoryEntity) {
		categoryDao.save(categoryEntity);
		return categoryEntity.getId();
	}

	public CategoryEntity loadBySubject(String categorySubject) {
		return categoryDao.loadBySubject(categorySubject);
	}

	public List<CategoryEntity> findAll() {
		return categoryDao.findAll();
	}

	public List<Sale> getTotalSales(String subject) {
		CategoryEntity categoryEntity = loadBySubject(subject);
		if (categoryEntity == null)
			throw new BadArgumentException("no category with subject = '" + subject + "'");
		List<Sale> sales = new ArrayList<>();
		for (ProductEntity productEntity : categoryEntity.getProducts()) {
			Sale sale = new Sale();
			sale.setProductCode(productEntity.getCode());
			sale.setToalQuantity(productManager.getTotalSale(productEntity));
			sales.add(sale);
		}
		return sales;
	}

}
