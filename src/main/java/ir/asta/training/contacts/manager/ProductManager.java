package ir.asta.training.contacts.manager;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import ir.asta.training.contacts.dao.ProductDao;
import ir.asta.training.contacts.entities.CategoryEntity;
import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.ProductEntity;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;

@Named("productManager")
@Transactional
public class ProductManager {

	@Inject
	private ProductDao productDao;
	@Inject
	private CategoryManager categoryManager;
	private Logger logger = LoggerFactory.getLogger("ir.asta.training.contacts");
	
	public Long save(ProductEntity product) {
		productDao.save(product);
		logger.info("new product saved");
		return product.getId();
	}

	public List<CategoryEntity> setCategories(String productCode, Map<String, String[]> params) {
		ProductEntity productEntity = productDao.loadByCode(productCode);
		if (productEntity == null) {
			throw new BadArgumentException("no product with code = '" + productCode + "'");
		}
		productEntity.clearCategories();
		String[] categories;
		if ((categories = params.get("category")) != null) {
			for (String categorySubject : categories) {
				CategoryEntity categoryEntity = categoryManager.loadBySubject(categorySubject);
				if (categoryEntity == null) {
					throw new BadArgumentException("no category with subject = '" + categorySubject + "'");
				}
				productEntity.addCategory(categoryEntity);
				categoryEntity.addProduct(productEntity);
			}
		}
		return getCategories(productCode);
	}

	public List<CategoryEntity> getCategories(String productCode) {
		return productDao.getCategories(productCode);
	}

	public List<ProductEntity> findAll() {
		return productDao.findAll();
	}

	public ProductEntity loadByCode(String code) {
		return productDao.loadByCode(code);
	}

	public Integer getTotalSale(ProductEntity productEntity) {
		int totalSale = 0;
		List<ItemEntity> allItems = productDao.getRelatedItems(productEntity);
		for (ItemEntity itemEntity : allItems) {
			totalSale += itemEntity.getQuantity();
		}
		return totalSale;
	}

}
