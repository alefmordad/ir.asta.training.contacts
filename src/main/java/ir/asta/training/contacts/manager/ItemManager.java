package ir.asta.training.contacts.manager;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import ir.asta.training.contacts.dao.ItemDao;
import ir.asta.training.contacts.entities.ItemEntity;

@Named("itemManager")
@Transactional
public class ItemManager {

	@Inject
	private ItemDao itemDao;

	public Long save(ItemEntity item) {
		itemDao.save(item);
		return item.getId();
	}

	public List<ItemEntity> findAll() {
		return itemDao.findAll();
	}

	public List<ItemEntity> findInInterval(Date fromDate, Date toDate) throws ParseException {
		return itemDao.findInInterval(fromDate, toDate);
	}

}
