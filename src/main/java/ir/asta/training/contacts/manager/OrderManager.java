package ir.asta.training.contacts.manager;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import ir.asta.training.contacts.dao.OrderDao;
import ir.asta.training.contacts.entities.ItemEntity;
import ir.asta.training.contacts.entities.OrderEntity;
import ir.asta.training.contacts.entities.ProductEntity;
import ir.asta.training.contacts.manager.exceptions.BadArgumentException;

@Named("orderManager")
@Transactional
public class OrderManager {

	@Inject
	private OrderDao orderDao;
	@Inject
	private ItemManager itemManager;
	@Inject
	private ProductManager productManager;

	public OrderEntity saveOrder(Date purchaseTime, List<ItemEntity> items) {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setPurchaseTime(purchaseTime);
		checkItemsValidation(items);
		items.forEach(item -> itemManager.save(item));
		orderEntity.setItems(items);
		orderDao.save(orderEntity);
		return orderEntity;
	}

	private void checkItemsValidation(List<ItemEntity> items) {
		for (ItemEntity item : items) {
			String unvalidProductCode = item.getProduct().getCode();
			ProductEntity validPoductEntity = productManager.loadByCode(unvalidProductCode);
			if (validPoductEntity == null) {
				throw new BadArgumentException("no product with code = '" + unvalidProductCode + "'");
			}
			item.setProduct(validPoductEntity);
		}
	}

	public List<OrderEntity> findAll() {
		return orderDao.findAll();
	}

	public List<OrderEntity> findInInterval(Date fromDate, Date toDate) {
		return orderDao.findInInterval(fromDate, toDate);
	}

}
