package ir.asta.training.contacts.manager;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import ir.asta.training.contacts.dao.EventDao;
import ir.asta.training.contacts.entities.EventEntity;

@Named("eventManager")
public class EventManager {

	@Inject
	private EventDao eventDao;
	
	@Transactional
	public void save(EventEntity entity) {
		eventDao.save(entity);
	}

	public EventEntity load(Long id) {
		return eventDao.load(id);
	}
	
	public List<EventEntity> findAll() {
		return eventDao.findAll();
	}
	
	@Transactional
	public void update(EventEntity entity) {
		eventDao.update(entity);
	}
	
	@Transactional
	public void delete(long id) {
		eventDao.delete(id);
	}
	
	public List<EventEntity> search(Map<String, String[]> params) {
		return eventDao.search(params);
	}

}
