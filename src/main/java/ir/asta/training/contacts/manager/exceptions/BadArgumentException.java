package ir.asta.training.contacts.manager.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ir.asta.wise.core.datamanagement.ActionResult;

@SuppressWarnings("serial")
public class BadArgumentException extends WebApplicationException {

	public BadArgumentException(String msg) {
		super(Response.status(Response.Status.NOT_FOUND).entity(new ActionResult<Object>(msg)).type(MediaType.APPLICATION_JSON).build());
	}

}
